package net.resiststan.core.multithreading.waitnotifynotifyall;

public abstract class MessageHandler extends Tag implements Runnable {
    protected final Message message;

    public MessageHandler(Message message) {
        this.message = message;
    }

    abstract void handle();

    @Override
    public void run() {
        synchronized (message) {
            try {
                Thread.sleep(1000);
                System.out.println(getTag() + ": waiting for notification " + System.currentTimeMillis());
                message.wait();

                System.out.println(getTag() + ": caught notify of another thread " + System.currentTimeMillis());

                System.out.println(getTag() + ": handle message...");
                Thread.sleep(1000);
                handle();
                System.out.println(getTag() + ": handle done");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
