package net.resiststan.core.multithreading.waitnotifynotifyall;

public class TitleMessageHandler extends MessageHandler {
    public TitleMessageHandler(Message message) {
        super(message);
    }

    @Override
    public void handle() {
        long id = message.getId();
        String title = message.getTitle();

        if (title == null) {
            System.out.println(String.format("%s: In message with id %s title is undefined", getTag(), id));
        } else {
            System.out.println(String.format("%s: In message with id %s title has length %s", getTag(), id, title.length()));
        }
    }
}
