package net.resiststan.core.multithreading.waitnotifynotifyall;

public abstract class Tag {
    protected final String getTag() {
        return String.format("[%s - in thread %s]", this.getClass().getSimpleName(), Thread.currentThread().getName());
    }
}
