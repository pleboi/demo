package net.resiststan.core.immutableobject;

import java.util.Objects;

public class ImmutableObject implements Cloneable {
    private final Long longField;
    private final String stringField;
    private final MutableObject objectField;

    public ImmutableObject(Long longField, String stringField, MutableObject objectField) {
        this.longField = longField;
        this.stringField = stringField;
        this.objectField = objectField;
    }

    public Long getLongField() {
        return longField;
    }

    public String getStringField() {
        return stringField;
    }

    public MutableObject getObjectField() {
        return new MutableObject(objectField);
    }

    @Override
    protected Object clone() {
        return new ImmutableObject(getLongField(), getStringField(), getObjectField());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ImmutableObject that = (ImmutableObject) o;
        return Objects.equals(longField, that.longField) &&
                Objects.equals(stringField, that.stringField) &&
                Objects.equals(objectField, that.objectField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(longField, stringField, objectField);
    }

    @Override
    public String toString() {
        return "ImmutableObject{" +
                "longField=" + longField +
                ", stringField='" + stringField + '\'' +
                ", objectField=" + objectField +
                '}';
    }
}
