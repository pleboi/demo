package net.resiststan.core.immutableobject;

import java.util.Objects;

public class MutableObject {
    private int intField;
    private long longField;
    private String stringField;

    public MutableObject() {
    }

    public MutableObject(int intField, long longField, String stringField) {
        this.intField = intField;
        this.longField = longField;
        this.stringField = stringField;
    }

    public MutableObject(MutableObject objectField) {
        this.intField = objectField.getIntField();
        this.longField = objectField.getLongField();
        this.stringField = objectField.getStringField();
    }

    public int getIntField() {
        return intField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }

    public long getLongField() {
        return longField;
    }

    public void setLongField(long longField) {
        this.longField = longField;
    }

    public String getStringField() {
        return stringField;
    }

    public void setStringField(String stringField) {
        this.stringField = stringField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MutableObject that = (MutableObject) o;
        return intField == that.intField &&
                longField == that.longField &&
                Objects.equals(stringField, that.stringField);
    }

    @Override
    public int hashCode() {

        return Objects.hash(intField, longField, stringField);
    }

    @Override
    public String toString() {
        return "MutableObject{" +
                "intField=" + intField +
                ", longField=" + longField +
                ", stringField='" + stringField + '\'' +
                '}';
    }
}
