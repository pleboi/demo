package net.resiststan.patterns.structural.facade.restoran;

import java.util.List;

public class Restaurant {
    List<Product> products = ProductsFabric.getProducts();
    Waiter waiter = new Waiter();
    Cook chefCook = new ChefCook();
    Cook assistantCook = new AssistantCook();


    public Lunch getLunch(Order order) {
        waiter.getOrder(order);
        waiter.takeOrder(chefCook);
        Recipe recipe = chefCook.makeRecipe();
        chefCook.giveAssignmentTo(assistantCook);
        List<Product> preparedProducts = assistantCook.prepareProduct();

        return assistantCook.make(order, preparedProducts, recipe);
    }
}
