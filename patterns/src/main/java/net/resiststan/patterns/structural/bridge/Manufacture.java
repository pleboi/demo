package net.resiststan.patterns.structural.bridge;

public interface Manufacture {
    default String getName() {
        return this.getClass().getSimpleName();
    }
}
