package net.resiststan.patterns.structural.composite;

public interface Component {
    default Class<? extends Component> getType() {
        return this.getClass();
    }
}
