package net.resiststan.patterns.structural.bridge.vehiceltype;

import net.resiststan.patterns.structural.bridge.Manufacture;
import net.resiststan.patterns.structural.bridge.Vehicle;

public class Bus extends Vehicle {
    public Bus(Manufacture manufacture) {
        super(manufacture);
    }
}
