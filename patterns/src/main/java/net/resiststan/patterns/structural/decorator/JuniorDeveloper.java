package net.resiststan.patterns.structural.decorator;

public class JuniorDeveloper implements Developer {
    @Override
    public void makeJob() {
        writeCode();
    }

    private void writeCode() {
        System.out.println("Write code.");
    }
}
