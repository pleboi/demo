package net.resiststan.patterns.structural.facade;

import net.resiststan.patterns.structural.facade.restoran.Order;
import net.resiststan.patterns.structural.facade.restoran.Restaurant;

/*
Шаблон: Фасад (Facade)

Цель:
- Предоставить унифицированный интерфейс вместо нескольких интерфейсов подститсемы.

Для чего используется:
- Используется для опредления интерфейса высокого уровня, который упрощает использование подсистемы.

Пример использования:
- изолирование клиентов от компонентов подсистемы, упрощая работу с ней;
- необходимость ослабления связанности подсистемы с клиентами;
*/
public class DemoFacade {
    public static void main(String[] args) {
        Restaurant restaurant = new Restaurant();
        restaurant.getLunch(new Order());
    }
}
