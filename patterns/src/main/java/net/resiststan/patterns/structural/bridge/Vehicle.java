package net.resiststan.patterns.structural.bridge;

public abstract class Vehicle {
    protected Manufacture manufacture;

    public Vehicle(Manufacture manufacture) {
        this.manufacture = manufacture;
    }

    public void drive() {
        System.out.println("Drive " + this.getClass().getSimpleName() + " like " + manufacture.getName());
    }
}
