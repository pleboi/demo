package net.resiststan.patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class WriterDepartment {
    private static Map<String, Writer> writers = new HashMap<>();


    public static Writer get(String type) {
        Writer writer = writers.get(type);

        if (writer == null) {
            if (type.equalsIgnoreCase("Novelist")) {
                System.out.println("Hiring novel writer.");
                writer = new Novelist();
            } else if (type.equalsIgnoreCase("Fable writer")) {
                System.out.println("Hiring fable writer.");
                writer = new FableWriter();
            }

            writers.put(type, writer);
        }

        return writer;
    }
}
