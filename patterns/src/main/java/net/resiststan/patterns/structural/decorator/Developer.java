package net.resiststan.patterns.structural.decorator;

public interface Developer {
    void makeJob();
}
