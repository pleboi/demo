package net.resiststan.patterns.structural.flyweight;

public interface Writer {
    void doWork();
}
