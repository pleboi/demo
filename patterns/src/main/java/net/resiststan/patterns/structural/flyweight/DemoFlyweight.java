package net.resiststan.patterns.structural.flyweight;

/*
Шаблон: Приспособленец (Flyweight)

Цель:
- Поддержка множества мелких объектов.

Для чего используется:
- Использует разделение для того, чтобы поддерживать много мелких объектов.

Пример использования:
- когда используется большое число объектов;
- большую часть состояния объектов можно вынести наружу;
- приложение не зависит от идентичности объекта
*/
public class DemoFlyweight {
    public static void main(String[] args) {
        Writer writer1 = WriterDepartment.get("Novelist");
        Writer writer2 = WriterDepartment.get("Novelist");
        Writer writer3 = WriterDepartment.get("Novelist");
        Writer writer4 = WriterDepartment.get("Fable writer");
        Writer writer5 = WriterDepartment.get("Fable writer");

        writer1.doWork();
        writer2.doWork();
        writer3.doWork();
        writer4.doWork();
        writer5.doWork();

        System.out.println(writer1 == writer2);
    }
}
