package net.resiststan.patterns.structural.composite;

import java.util.List;

public interface Composite extends Component {
    List<Component> getChildren();

    boolean add(Component component);

    boolean remove(Component component);
}
