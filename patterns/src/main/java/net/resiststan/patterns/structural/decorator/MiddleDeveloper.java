package net.resiststan.patterns.structural.decorator;

public class MiddleDeveloper extends DeveloperDecorator {

    MiddleDeveloper(Developer developer) {
        super(developer);
    }

    @Override
    public void makeJob() {
        getReady();
        developer.makeJob();
    }

    private void getReady() {
        System.out.println("Reed documentation.");
    }
}
