package net.resiststan.patterns.structural.facade.restoran;

import java.util.List;

public interface Cook {
    Recipe makeRecipe();

    void giveAssignmentTo(Cook assistantCook);

    List<Product> prepareProduct();

    Lunch make(Order order, List<Product> preparedProducts, Recipe recipe);

}
