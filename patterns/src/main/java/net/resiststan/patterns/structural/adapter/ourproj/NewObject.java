package net.resiststan.patterns.structural.adapter.ourproj;

public interface NewObject {
    void create();

    void read();

    void update();

    void delete();
}
