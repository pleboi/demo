package net.resiststan.patterns.structural.facade.restoran;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ChefCook implements Cook {
    @Override
    public Recipe makeRecipe() {
        return new Recipe();
    }

    @Override
    public void giveAssignmentTo(Cook assistantCook) {

    }

    @Override
    public List<Product> prepareProduct() {
        return Arrays.asList(new Product(), new Product());
    }

    @Override
    public Lunch make(Order order, List<Product> preparedProducts, Recipe recipe) {
        return new Lunch();
    }
}
