package net.resiststan.patterns.structural.composite;

/*
Шаблон: Компоновщик (Composite)

Цель:
- Скомпонировать объекты в структуры по типу "дерева", позволяя клиентам единообразно
трактовать отдельные и составные объекты.

Для чего используется:
- Для группировки мелких компонентов в более крупные, которые, в свою очередь, могут
стать основой для ещё более крупных стуктур.

Пример использования:
- для представления иерархии "часть-целое";
- мы хотим, чтобы клиенты одним способом трактовали как отдельные, так и составные объекты.
*/
public class DemoComposite {
    public static void main(String[] args) {
        Box bigBox = new Box();
        Component product = new Product("Receipt");
        bigBox.add(product);
        System.out.println(product.getType());

        Box box1 = new Box();
        bigBox.add(box1);
        box1.add(new Product("Product_1"));
        box1.add(new Product("Product_2"));
        box1.add(new Product("Product_3"));

        Box box2 = new Box();
        bigBox.add(box2);
        Product item_1 = new Product("Item_1");
        box2.add(item_1);
        box2.add(new Product("Item_2"));
        box2.add(new Product("Item_3"));
        box2.add(new Product("Item_4"));
        box2.add(new Product("Item_5"));
        box2.add(new Product("Item_6"));
        box2.add(new Product("Item_7"));

        System.out.println(bigBox);

        box2.remove(item_1);
        System.out.println(bigBox);

    }
}
