package net.resiststan.patterns.creational.abstractfactory;

public interface Furniture {
    default void printName() {
        System.out.println(this.getClass().getSimpleName() + "-" + this.hashCode());
    }
}
