package net.resiststan.patterns.creational.prototype;

public abstract class Shape implements Prototype<Shape> {
    private int x;
    private int y;
    private String color;

    public Shape() {}

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    protected Shape(Shape src) {
        if (src != null) {
            this.x = src.x;
            this.y = src.y;
            this.color = src.color;
        }
    }
}
