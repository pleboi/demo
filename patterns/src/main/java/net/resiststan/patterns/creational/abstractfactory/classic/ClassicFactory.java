package net.resiststan.patterns.creational.abstractfactory.classic;

import net.resiststan.patterns.creational.abstractfactory.Chair;
import net.resiststan.patterns.creational.abstractfactory.FurnitureFactory;
import net.resiststan.patterns.creational.abstractfactory.Sofa;
import net.resiststan.patterns.creational.abstractfactory.Table;

public class ClassicFactory implements FurnitureFactory {
    @Override
    public Chair createChair() {
        return new ClassicChair();
    }

    @Override
    public Table createTable() {
        return new ClassicTable();
    }

    @Override
    public Sofa createSofa() {
        return new ClassicSofa();
    }
}
