package net.resiststan.patterns.creational.prototype;

public interface Prototype<T> {
    T copy();
}
