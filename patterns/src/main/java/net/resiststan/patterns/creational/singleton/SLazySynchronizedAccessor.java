package net.resiststan.patterns.creational.singleton;

public class SLazySynchronizedAccessor {
    private static SLazySynchronizedAccessor instance;

    public static synchronized SLazySynchronizedAccessor getInstance() {
        if (instance == null) {
            instance = new SLazySynchronizedAccessor();
        }

        return instance;
    }
}
