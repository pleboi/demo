package net.resiststan.patterns.creational.singleton;

public class SLazyDoubleChecked {
    private static volatile SLazyDoubleChecked instance;

    public static SLazyDoubleChecked getInstance() {
        SLazyDoubleChecked tmpInst = instance;

        if (tmpInst == null) {
            synchronized (SLazyDoubleChecked.class) {
                tmpInst = instance;

                if (tmpInst == null) {
                    tmpInst = instance = new SLazyDoubleChecked();
                }
            }
        }

        return tmpInst;
    }
}
