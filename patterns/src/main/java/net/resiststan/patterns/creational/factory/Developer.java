package net.resiststan.patterns.creational.factory;

public interface Developer {
    default void writeCode() {
        System.out.println(this.getClass().getSimpleName() + " writes code");
    }
}
