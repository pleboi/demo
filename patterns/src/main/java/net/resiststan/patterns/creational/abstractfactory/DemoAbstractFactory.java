package net.resiststan.patterns.creational.abstractfactory;

import net.resiststan.patterns.creational.abstractfactory.classic.ClassicFactory;
import net.resiststan.patterns.creational.abstractfactory.modern.ModernFactory;

/*
Шаблон: Абстрактная фабрика (Abstract Factory)

Цель:
- Создание интерфейса, для создания множества взаимосвязанных или взаимозависимых объектов, без жёсткой привязке к конкретным классам.

Для чего используется:
- Для создания множеств взаимосвязанных объектов.

Пример использования:
- система не должна зависеть от метода создания, компоновки и представления входящих в неё объектов;
- входящие взаимосвязанные объекты должны использоваться вместе;
- система должна конфигурироваться одним из множеств объектов, из которых она состоит;
- нам необходимо предоставить множество объектов, раскрывая только их интерфейсы но не реализацию.
*/
public class DemoAbstractFactory {
    public static void main(String[] args) {
        FurnitureFactory modernFactory = FurnitureFactory.getFactory(new ModernFactory());
        FurnitureFactory classicFactory = FurnitureFactory.getFactory(new ClassicFactory());

        Chair chair = modernFactory.createChair();
        Chair chair2 = modernFactory.createChair();
        chair.printName();
        chair2.printName();

        Table table = classicFactory.createTable();
        Sofa sofa = classicFactory.createSofa();
        Chair chair1 = classicFactory.createChair();

        table.printName();
        sofa.printName();
        chair1.printName();
    }
}
