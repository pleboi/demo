package net.resiststan.patterns.behavioral.strategy;

public class Handyman implements Painter {
    @Override
    public void paint() {
        System.out.println("Price: acceptable.");
        System.out.println("Quality: acceptable.");
        System.out.println("Speed: slow.");
    }
}
