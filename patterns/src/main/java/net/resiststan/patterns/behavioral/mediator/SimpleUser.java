package net.resiststan.patterns.behavioral.mediator;

public class SimpleUser implements User {
    private final Chat chat;
    private final String name;

    public SimpleUser(Chat chat, String name) {
        this.chat = chat;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message) {
        System.out.println(this.name + " sending: " + message);
        chat.sendMessage(this, message);
    }

    @Override
    public void getMessage(String message) {
        System.out.println(this.name + " receiving: " + message);
    }
}
