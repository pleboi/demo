package net.resiststan.patterns.behavioral.command;

public class Buy implements Command {
    private Vehicle vehicle;

    public Buy(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public void execute() {
        System.out.println("Buy.");
    }
}
