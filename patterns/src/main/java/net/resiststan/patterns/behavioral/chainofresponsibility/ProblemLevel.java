package net.resiststan.patterns.behavioral.chainofresponsibility;

public enum ProblemLevel {
    LOW(1),
    MID(2),
    HIGH(3);

    int level;

    ProblemLevel(int level) {
        this.level = level;
    }
}
