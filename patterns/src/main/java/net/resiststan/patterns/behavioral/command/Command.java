package net.resiststan.patterns.behavioral.command;

public interface Command {
    void execute();
}
