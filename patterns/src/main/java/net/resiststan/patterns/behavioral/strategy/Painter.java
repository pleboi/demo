package net.resiststan.patterns.behavioral.strategy;

public interface Painter {
    void paint();
}
