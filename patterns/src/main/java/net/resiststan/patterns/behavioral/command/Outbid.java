package net.resiststan.patterns.behavioral.command;

public class Outbid {
    Command byu;
    Command inspect;
    Command repair;
    Command sell;

    public Outbid(Command byu, Command inspect, Command repair, Command sell) {
        this.byu = byu;
        this.inspect = inspect;
        this.repair = repair;
        this.sell = sell;
    }

    public void buyCar() {
        byu.execute();
    }

    public void inspectCar() {
        inspect.execute();
    }

    public void repairCar() {
        repair.execute();
    }

    public void sellCar() {
        sell.execute();
    }
}
