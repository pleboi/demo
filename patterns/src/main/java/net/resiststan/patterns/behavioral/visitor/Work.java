package net.resiststan.patterns.behavioral.visitor;

public interface Work {
    void doWork(Visitor visitor);
}
