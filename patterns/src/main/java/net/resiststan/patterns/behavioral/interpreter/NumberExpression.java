package net.resiststan.patterns.behavioral.interpreter;

public class NumberExpression implements Expression<Integer, String> {
    @Override
    public Integer interpret(String context) {
        String substring = context.substring(0, context.length() - 1);
        try {
            return Integer.valueOf(substring);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
