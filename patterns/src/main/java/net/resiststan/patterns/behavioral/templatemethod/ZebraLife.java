package net.resiststan.patterns.behavioral.templatemethod;

public class ZebraLife extends AnimalLifeTemplate {
    @Override
    void eat() {
        System.out.println("Go to meadow to eat grass.");
    }

    @Override
    void routine() {
        System.out.println("Be alert.");
        System.out.println("Do not be caught by lion.");
    }
}
