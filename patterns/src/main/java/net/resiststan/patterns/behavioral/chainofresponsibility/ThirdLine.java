package net.resiststan.patterns.behavioral.chainofresponsibility;

public class ThirdLine extends Handler {
    public ThirdLine(ProblemLevel problemLevel) {
        super(problemLevel);
    }

    @Override
    void resolved(Call call) {
        System.out.println(this.getClass().getSimpleName() + " try resolved problem...");
    }
}
