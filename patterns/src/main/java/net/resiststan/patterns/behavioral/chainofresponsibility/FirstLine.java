package net.resiststan.patterns.behavioral.chainofresponsibility;

public class FirstLine extends Handler {
    public FirstLine(ProblemLevel problemLevel) {
        super(problemLevel);
    }

    @Override
    void resolved(Call call) {
        System.out.println(this.getClass().getSimpleName() + " try resolved problem...");
    }
}
