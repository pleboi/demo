package net.resiststan.patterns.behavioral.state;

public class LowerCaseState implements State {
    @Override
    public State nextState() {
        return null;
    }

    @Override
    public void doAction(Context context) {
        Field field = (Field)context;
        field.setText(field.getText().toLowerCase());
    }
}
