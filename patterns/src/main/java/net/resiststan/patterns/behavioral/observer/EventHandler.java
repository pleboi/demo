package net.resiststan.patterns.behavioral.observer;

public class EventHandler implements Subscriber {
    private final String name;

    public EventHandler(String name) {
        this.name = name;
    }

    @Override
    public void update(String event) {
        System.out.println(name + " get event " + event);
    }

    @Override
    public String toString() {
        return "EventHandler{" +
                "name='" + name + '\'' +
                '}';
    }
}
