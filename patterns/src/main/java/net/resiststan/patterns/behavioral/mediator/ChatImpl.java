package net.resiststan.patterns.behavioral.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatImpl implements Chat {
    private List<User> users = new ArrayList<>();

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public void sendMessage(User user, String message) {
        for (User u : users) {
            if (u != user) {
                u.getMessage(message);
            }
        }
    }
}

