package net.resiststan.patterns.behavioral.visitor;

public interface Visitor {
    void wash(Washing washing);

    void cook(Cooking cooking);

    void clean(Cleaning cleaning);
}
