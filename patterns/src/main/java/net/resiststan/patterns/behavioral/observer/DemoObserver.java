package net.resiststan.patterns.behavioral.observer;

/*
Шаблон: Наблюдатель (Observer)

Цель:
- Определение зависимости "один ко многим" между объектами.

Для чего используется:
- Определение зависимости "один ко многим" между объектами таким образом,
что при изменении состояния одного объекта все зависящие от него объекты были уведомлены об этом и обновились.

Пример использования:
- когда у модели имеются два аспекта, один из которых зависит от другого. Инкапсулирование этих

аспектов в разные классы позволяют использовать их независимо друг от друга;
- когда один объект должен оповещать другие и не делать предположений об этих объектах;
- ослабление связи между объектами.
*/
public class DemoObserver {
    public static void main(String[] args) {
        EventPublisher eventPublisher = new EventPublisher();

        EventHandler subscriber1 = new EventHandler("EH-1");
        eventPublisher.addSubscriber(subscriber1);
        eventPublisher.addSubscriber(new EventHandler("EH-2"));

        eventPublisher.newEvent("new event 1");

        eventPublisher.addSubscriber(new EventHandler("EH-3"));
        eventPublisher.removeSubscriber(subscriber1);

        eventPublisher.newEvent("new event 2");
        eventPublisher.newEvent("new event 3");
    }
}
