package net.resiststan.patterns.behavioral.observer;

public interface Subscriber {
    void update(String event);
}
