package net.resiststan.patterns.behavioral.chainofresponsibility;

public class SecondLine extends Handler {
    public SecondLine(ProblemLevel problemLevel) {
        super(problemLevel);
    }

    @Override
    void resolved(Call call) {
        System.out.println(this.getClass().getSimpleName() + " try resolved problem...");
    }
}
