package net.resiststan.patterns.behavioral.memento;

import java.util.Deque;
import java.util.ArrayDeque;

public class OriginalObject {
    private String state;

    private final Deque<Memento> stack = new ArrayDeque<>();

    public OriginalObject(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento save() {
        Memento tmp = new Memento(this.state);
        stack.addLast(tmp);
        return tmp;
    }

    public void load() {
        Memento last = stack.pollLast();
        if (last != null) {
            state = last.getState();
        }
    }

    private static class Memento extends OriginalObject {
        public Memento(String state) {
            super(state);
        }
    }

    @Override
    public String toString() {
        return "OriginalObject{" +
                "state='" + state + '\'' +
                '}';
    }
}
