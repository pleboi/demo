package net.resiststan.patterns.behavioral.memento;

/*
Шаблон: Хранитель (Memento)

Цель:
- Сохранить внутренне состояние объекта за его пределы.

Для чего используется:
- Фиксирование внутреннего состояния объекта за его пределами не нарушая
инкапсуляцию и восстановления объекта в случае необходимости.

Пример использования:
- необходимо сохранить текущее состояние объекта или его части и восстановление в
будущем, но прямое получение состояния раскрывает детали реализации и нарушает инкапсуляцию объекта.
*/
public class DomeMemento {
    public static void main(String[] args) {
        OriginalObject object = new OriginalObject("version 1");
        object.save();
        System.out.println(object);

        object.setState("version 2");
        object.save();
        System.out.println(object);

        object.setState("version 3");
        object.save();
        System.out.println(object);

        object.setState("version 4");
        System.out.println(object);

        object.load();
        System.out.println(object);

        object.load();
        System.out.println(object);

        object.load();
        System.out.println(object);

        object.load();
        System.out.println(object);
    }
}
