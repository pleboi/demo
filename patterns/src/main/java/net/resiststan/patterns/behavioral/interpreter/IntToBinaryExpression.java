package net.resiststan.patterns.behavioral.interpreter;

public class IntToBinaryExpression implements Expression<String, String> {
    Expression<Integer, String> expression;

    public IntToBinaryExpression(Expression<Integer, String> expression) {
        this.expression = expression;
    }

    @Override
    public String interpret(String context) {
        return parse(expression.interpret(context));
    }

    private String parse(Integer context) {
        return Integer.toBinaryString(context);
    }
}
