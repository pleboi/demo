package net.resiststan.patterns.behavioral.visitor;

/*
Шаблон: Посетитель (Visitor)

Цель:
- Описание действий, которые выполняются с каждым объектом в некоторой структуре

Для чего используется:
- Описание операций, которые выполняются с каждым объектом из некоторой структуры.
Позволяет определить новую операцию без изменения классов этих объектов.

Пример использования:
- в структуре присутствуют объекты многих классов с различными интерфейсами и нам необходимо
выполнить над ними операции, которые зависят от конкретных классов;
- необходимо выполнять не связанные между собой операции над объектами, которые входят в
состав структуры и мы не хотим добавлять эти операции в классы;
- классы, которые утсанавливают структуру объектов редко изменяются, но часто добавляются
новые операции над этой структурой.
*/
public class DemoVisitor {
    public static void main(String[] args) {
        Home home = new Home(new Work[]{new Washing(), new Cleaning(), new Cooking()});
        Visitor girlfriend = new Girlfriend();
        Visitor mum = new Mum();

        home.doWork(girlfriend);
        home.doWork(mum);
    }
}
