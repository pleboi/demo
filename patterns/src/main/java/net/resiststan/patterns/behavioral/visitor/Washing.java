package net.resiststan.patterns.behavioral.visitor;

public class Washing implements Work {
    @Override
    public void doWork(Visitor visitor) {
        visitor.wash(this);
    }
}
