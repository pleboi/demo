package net.resiststan.patterns.behavioral.mediator;

/*
Шаблон: Посредник (Mediator)

Цель:
- Инкапсуляция способа взаимодейтсвия множества объектов

Для чего используется:
- Для определения объекта, который инкапсулирует способ взаимодействия множества
объектов и обеспечения слабой связи между этими объектами.

Пример использования:
- связи между объектами сложны и чётко определены;
- нельзя повторно использовать объект, так как он обменивается информацией с другими объектами;
- поведение, распределённое между несколькими классами должно легко настраиваться без создания подклассов.
*/
public class DemoMediator {
    public static void main(String[] args) {
        Chat chat = new ChatImpl();

        User user1 = new SimpleUser(chat, "User1");
        User user2 = new SimpleUser(chat, "User2");
        User user3 = new SimpleUser(chat, "User3");
        User user4 = new SimpleUser(chat, "User4");

        chat.addUser(user1);
        chat.addUser(user2);
        chat.addUser(user3);
        chat.addUser(user4);

        user3.sendMessage("new message.");
    }
}
