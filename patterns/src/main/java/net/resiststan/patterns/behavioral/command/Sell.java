package net.resiststan.patterns.behavioral.command;

public class Sell implements Command {
    private Vehicle vehicle;

    public Sell(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public void execute() {
        System.out.println("Sell.");
    }
}
