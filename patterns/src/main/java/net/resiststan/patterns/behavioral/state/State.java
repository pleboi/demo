package net.resiststan.patterns.behavioral.state;

public interface State {
    State nextState();

    void doAction(Context context);
}
