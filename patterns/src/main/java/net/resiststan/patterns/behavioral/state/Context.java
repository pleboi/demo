package net.resiststan.patterns.behavioral.state;

public interface Context {
    void setState(State state);

    State getState();

    void changeState();
}
