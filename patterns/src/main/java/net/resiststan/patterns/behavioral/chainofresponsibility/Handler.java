package net.resiststan.patterns.behavioral.chainofresponsibility;

public abstract class Handler {
    private Handler nextHandler;
    private ProblemLevel problemLevel;

    public Handler(ProblemLevel problemLevel) {
        this.problemLevel = problemLevel;
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handle(Call call) {
        if (problemLevel.level <= call.getProblemLevel().level) {
            resolved(call);

        }

        if (nextHandler != null) {
            nextHandler.handle(call);
        }
    }

    abstract void resolved(Call call);
}
