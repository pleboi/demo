package net.resiststan.patterns.behavioral.chainofresponsibility;

/*
Шаблон: Цепочка ответственности (Chain Of Responsibility)

Цель:
- Связывание объектов-получаетлей в цепочку и передача запроса по ней.

Для чего используется:
- Помогает избежать привязки отправителя запроса к его получателю, что даёт
возможность обработать данный запрос нескольким объектам.

Пример использования:
- ослабление привязанности (объект не должен знать, кто именно обработает его запрос);
- дополнительная гибкость при распределении обязанностей между объектами;
*/
public class DemoChainOfResponsibility {
    public static void main(String[] args) {
        Handler firstLine = new FirstLine(ProblemLevel.LOW);
        Handler secondLine = new SecondLine(ProblemLevel.MID);
        Handler thirdLine = new ThirdLine(ProblemLevel.HIGH);

        firstLine.setNextHandler(secondLine);
        secondLine.setNextHandler(thirdLine);

        firstLine.handle(new Call(ProblemLevel.HIGH));
        System.out.println();
        firstLine.handle(new Call(ProblemLevel.MID));
        System.out.println();
        firstLine.handle(new Call(ProblemLevel.LOW));
    }
}
