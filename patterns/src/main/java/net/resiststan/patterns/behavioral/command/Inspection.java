package net.resiststan.patterns.behavioral.command;

public class Inspection implements Command {
    private Vehicle vehicle;

    public Inspection(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public void execute() {
        System.out.println("Inspection.");
    }
}
