package net.resiststan.patterns.behavioral.visitor;

public class Mum implements Visitor {
    @Override
    public void wash(Washing washing) {
        System.out.println("Washing like " + this.getClass().getSimpleName());
    }

    @Override
    public void cook(Cooking cooking) {
        System.out.println("Cooking like " + this.getClass().getSimpleName());
    }

    @Override
    public void clean(Cleaning cleaning) {
        System.out.println("Cleaning like " + this.getClass().getSimpleName());
    }
}
