package net.resiststan.patterns.behavioral.iterator;

import java.util.Arrays;

public class SomeCollection<T> implements Collection<T> {
    private T[] arr;

    public SomeCollection() {
    }

    public SomeCollection(T[] arr) {
        this.arr = arr;
    }

    public void add(T t) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = t;
    }

    public T get(int i) {
        return arr[i];
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            int index;

            @Override
            public boolean hasNext() {
                return index < arr.length;
            }

            @Override
            public T next() {
                return arr[index++];
            }
        };
    }
}
