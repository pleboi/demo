package net.resiststan.patterns.behavioral.command;

public class Repair implements Command {
    private Vehicle vehicle;

    public Repair(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public void execute() {
        System.out.println("Repair.");
    }
}
