package net.resiststan.tasks.wordmatrix;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class WordMatrixTest {
    private static Matrix matrix;
    private static char[][] wordMatrix;

    @Parameter(value = 0)
    public String word;
    @Parameter(value = 1)
    public boolean isContains;


    @Parameters(name = "{index}: Word \"{0}\" contains in matrix - {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"HABR", true},
                {"HABRA", false},
                {"FOR", false},
                {"QUIZ", true},
                {"GO", false},
        });
    }

    @BeforeClass
    public static void setUp() {
        wordMatrix = new char[][]{{'H', 'I', 'Z'},
                {'U', 'A', 'R'},
                {'Q', 'N', 'B'}};
        matrix = new Matrix(wordMatrix);
    }

    @Test
    public void t() {
        assertEquals(isContains, matrix.isWord(word));
    }
}
