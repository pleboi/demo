package net.resiststan.tasks.calculator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculatorTest {
    static ICalc calculator;

    @Parameter(value = 0)
    public String expresion;
    @Parameter(value = 1)
    public double expResult;

    @Parameters(name = "{index}: {0}={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"(1+((2+3)*(4*5)))", 101.0},
                {"(1+18/3-2)*2", 10.0},
                {"(.1+.2 + 1.+2.) / 3", 1.1},
                {"1-2", -1.0},
                {"1--2", 3.0},
                {"-1--2", 1.0},
                {"+1--2", 3.0},
                {"+6-+5", 1.0},
                {"2+2*2", 6.0},
                {"+2-+3", -1.0},
                {"(5+1)*(-1+2)", 6.0},
                {"(5+1)*(-1)", -6.0},
                {"5+1*1+2", 8.0},
                {"4--2", 6.0},
                {"-4--2", -2.0},
                {"-(-1)-(-2)", 3.0},
                {"-(-(-1))-(-2)", 1.0},
                {"--1--2", 3.0},
                {"-1---2", -3.0},
                {"---1--2", 1.0},
                {"10/3", 3.33333}
        });
    }

    @BeforeClass
    public static void setUp() {
        calculator = new SCalculator();
    }

    @Test
    public void SCalculatorTest() {
        double eval = ExpressionParser.eval(calculator, expresion);
        Assert.assertEquals(expResult, eval, 1e-6);
    }
}
