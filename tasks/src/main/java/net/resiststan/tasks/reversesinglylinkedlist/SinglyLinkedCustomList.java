package net.resiststan.tasks.reversesinglylinkedlist;

public class SinglyLinkedCustomList<T> implements CustomList<T> {
    Node<T> root;
    Node<T> last;
    private long size = 0;

    SinglyLinkedCustomList() { }

    SinglyLinkedCustomList(T t) {
        createRoot(t);
    }

    public SinglyLinkedCustomList(T... t) {
        for (T v : t) {
            add(v);
        }
    }

    public Node<T> getRoot() {
        return root;
    }

    public Node<T> getLast() {
        return last;
    }

    public long getSize() {
        return size;
    }

    private void createRoot(T t) {
        root = new Node<>(t);
        last = root;
        size++;
    }

    private boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void add(T t) {
        if (isEmpty()) {
            createRoot(t);
        } else {
            Node<T> newNode = new Node<>(t);
            last.setNext(newNode);
            last = newNode;
            size++;
        }
    }

    @Override
    public void reverse() {
        if (isEmpty() || size == 1) {
            return;
        }

        Node<T> next = root;
        Node<T> res = newNode(root, null);
        while (next.getNext() != null) {
            next = next.getNext();
            res = newNode(next, res);
        }

        root = res;
    }

    private Node<T> newNode(Node<T> l, Node root) {
        Node<T> nn = new Node(l.getValue());
        nn.next = root;
        return nn;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString()).append("{");
        Node<T> node = this.root;

        if (isEmpty()) {
            sb.append((String) null);
        } else {
            do {
                sb.append(node);
                if (!node.hasNext()) {
                    break;
                }
                sb.append(", ");
                node = node.getNext();
            } while (true);
        }

        return sb.append("}").toString();
    }
}
