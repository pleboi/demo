package net.resiststan.tasks.reversesinglylinkedlist;

public class Main {

    public static void main(String args[]) {
        CustomList<Integer> list = new SinglyLinkedCustomList<>(1, 2, 3);
        list.add(12);
        list.add(23);

        System.out.println(list);
        list.reverse();
        System.out.println(list);

    }
}
