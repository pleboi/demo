package net.resiststan.tasks.reversesinglylinkedlist;

public interface CustomList<T> {
    void add(T t);

    void reverse();
}
