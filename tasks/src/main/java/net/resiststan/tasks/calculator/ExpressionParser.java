package net.resiststan.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

public class ExpressionParser {
    private static ICalc ic; //типа калькулятор которым считаем
    private static LinkedList<Double> st;// = new LinkedList < Double > (); // сюда наваливают цифры
    private static LinkedList<Character> op;// = new LinkedList < Character > (); // сюда опрераторы и st и op в порядке поступления
    private static int i;
    private static boolean bOper;
    private static int charCount;

    public static double eval(ICalc calcImp, String s) {
        //тут должна быть проверка что выражение вылидно
        ic = calcImp;
        st = new LinkedList<Double>(); // сюда наваливают цифры
        op = new LinkedList<Character>(); // сюда опрераторы и st и op в порядке поступления
        bOper = false; //флаг, который говорит что мы только что обработали оператор
        charCount = 0;

        for (i = 0; i < s.length(); i++) { // парсим строку с выражением и вычисляем
            char c = s.charAt(i);

            if (isDelim(c)) {
                continue;
            }

            if (c == '(') {
                processLeftBracet();
            } else if (c == ')') {
                processRightBracet();
            } else if (isOperator(c) && !bOper && st.size() > 0) {
                addOper(c);
            } else {
                prepareNumber(s);
            }
        }

        while (!op.isEmpty()) {
            processOperator(op.removeLast());
        }

        return BigDecimal.valueOf(st.get(0)).setScale(5, RoundingMode.HALF_UP).doubleValue();
    }

    private static boolean isDelim(char c) { // тру если пробел
        return c == ' ';
    }

    private static boolean isOperator(char c) { // возвращяем тру если один из символов ниже
        return c == '+'
                || c == '-'
                || c == '*'
                || c == '/'
                //|| c == '%'
                ;
    }

    private static int priority(char op) {
        switch (op) { // при + или - возврат 1, при * / % 2 иначе -1
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                //case '%':
                return 2;
            default:
                return -1;
        }
    }

    private static void processOperator(char op) {
        double r = st.removeLast(); // выдёргиваем из упорядоченного листа последний элемент
        double l = st.removeLast(); // также

        switch (op) { // выполняем действие между l и r в зависимости от оператора в кейсе и результат валим в st
            case '+':
                st.add(ic.Add(l, r));
                break;
            case '-':
                st.add(ic.Add(l, -r));
                break;
            case '*':
                st.add(ic.Multiply(l, r));
                break;
            case '/':
                st.add(ic.Divide(l, r));
                break;
            // case '%':
            // 	st.add(l % r);
            // 	break;
        }
    }

    private static void processLeftBracet() {
        op.add('(');
        bOper = true;
    }

    private static void processRightBracet() {
        while (op.getLast() != '(') {
            processOperator(op.removeLast());
        }

        op.removeLast();
        bOper = false;
    }

    private static void addOper(char c) {
        while (!op.isEmpty() && priority(op.getLast()) >= priority(c)) {
            processOperator(op.removeLast());
        }

        op.add(c);
        bOper = true;
    }

    private static void prepareNumber(String s) {
        StringBuilder operand = new StringBuilder();

        while (i < s.length() && ((Character.isDigit(s.charAt(i)) || s.charAt(i) == '.')
                || ((s.charAt(i) == '-' || s.charAt(i) == '+') && operand.length() == 0))
        ) {
            operand.append(s.charAt(i++));
        }

        if ((st.size() == 0 || bOper && charCount < 2 && st.size() > 0)
                && operand.length() == 1
                && (operand.toString().equals("-") || operand.toString().equals("+"))
        ) {
            operand.append("1");
            parseNum(operand.toString());

            if (bOper) {
                charCount = 0;
            } else {
                ++charCount;
            }

            addOper('*');
        } else {
            parseNum(operand.toString());

            charCount = 0;
        }

        --i;
    }

    private static void parseNum(String str) {
        try {
            st.add(Double.parseDouble(str));
        } catch (NumberFormatException e) {
            throw new RuntimeException("invalid expression " + str + " at char " + i);
        }

        bOper = false;
    }
}