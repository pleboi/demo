package net.resiststan.tasks.wordmatrix;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Matrix {
    private Position startPosition = null;
    private Position curentPosition = null;
    Set<Position> usedCharSet = new LinkedHashSet<Position>();

    private final char[][] charMatrix;

    public boolean isWord(String str) {
        setStart(str.charAt(0));

        if (getStart() != null) {
            usedCharSet.add(getCurent());
            for (int i = 1; i < str.length(); i++) {
                if (setNext(str.charAt(i)) && !usedCharSet.contains(getCurent())) {
                    usedCharSet.add(getCurent());
                } else {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    Matrix(char[][] charMatrix) {
        this.charMatrix = charMatrix;
    }

    private Position getStart() {
        return startPosition;
    }

    private Position getCurent() {
        return curentPosition;
    }

    private void setStart(char c) {
        exitlabel:
        for (int i = 0; i < charMatrix.length; i++) {
            for (int j = 0; j < charMatrix[i].length; j++) {
                if (charMatrix[i][j] == c) {
                    this.startPosition = new Position(i, j);
                    break exitlabel;
                }
            }
        }

        this.curentPosition = this.startPosition;
    }

    private boolean setNext(char c) {
        for (Position p : neighborList()) {
            try {
                if (charMatrix[p.X][p.Y] == c) {
                    curentPosition = new Position(p.X, p.Y);
                    return true;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                //
            }
        }

        curentPosition = null;
        return false;
    }

    private List<Position> neighborList() {
        return Arrays.asList(
                new Position(curentPosition.X - 1, curentPosition.Y - 1), //--
                new Position(curentPosition.X - 1, curentPosition.Y), //-0
                new Position(curentPosition.X - 1, curentPosition.Y + 1), //-+
                new Position(curentPosition.X, curentPosition.Y + 1), //0+
                new Position(curentPosition.X + 1, curentPosition.Y + 1), //++
                new Position(curentPosition.X + 1, curentPosition.Y), //+0
                new Position(curentPosition.X + 1, curentPosition.Y - 1), //+-
                new Position(curentPosition.X, curentPosition.Y - 1)  //0-
        );
    }
}

