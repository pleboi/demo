package net.resiststan.tasks.wordmatrix;

public class Position {
    public final int X;
    public final int Y;

    Position(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + X;
        result = prime * result + Y;

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Position other = (Position) obj;
        if (X != other.X) {
            return false;
        }

        if (Y != other.Y) {
            return false;
        }

        return true;
    }

    public String toString() {
        return X + " " + Y;
    }
}
