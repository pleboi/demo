package net.resiststan.tasks.stringsort1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class Impl {
        private static final Set<Character> alp = new HashSet<>(Arrays.asList(
                'ё', 'у', 'е', 'ы', 'а', 'о', 'э', 'я', 'и', 'ю',
                'Ё', 'У', 'Е', 'Ы', 'А', 'О', 'Э', 'Я', 'И', 'Ю'));

        public static void sort(String[] a) {
            sort(a, (s1, s2) -> {
                //TODO return 1 if rhs should be before s1
                //     return -1 if lhs should be before s2
                //     return 0 otherwise
                if (s1.equalsIgnoreCase(s2)) {
                    return 0;
                }

                int v = compareVowel(s1, s2);
                int l = compareLength(s1, s2);

                if (v == 0) {
                    return l;
                }

                return v;
            });
        }

        private static int compareLength(String s1, String s2) {
            return s1.length() - s2.length();
        }

        private static int compareVowel(String s1, String s2) {
            int max = Math.max(s1.length(), s2.length());

            int v1 = 0;
            int v2 = 0;

            for (int i = 0; i < max; i++) {
                if (i < s1.length() && alp.contains(s1.charAt(i))) {
                    v1++;
                }

                if (i < s2.length() && alp.contains(s2.charAt(i))) {
                    v2++;
                }
            }

            return v1 - v2;
        }

        public static void sort(String[] a, Comparator<String> c) {
            for (int i = 0; i < a.length; i++) {
                for (int j = i + 1; j < a.length; j++) {
                    if (c.compare(a[i], a[j]) <= 0) {
                        String tmp = a[i];
                        a[i] = a[j];
                        a[j] = tmp;
                    }
                }
            }
        }
    }


