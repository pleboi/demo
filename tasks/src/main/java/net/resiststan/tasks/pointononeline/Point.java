package net.resiststan.tasks.pointononeline;

import java.util.Objects;

public class Point implements Comparable<Point> {
    public final int X;
    public final int Y;

    public Point(int x, int y) {
        X = x;
        Y = y;
    }

    public int compareTo(Point o) {
        if (this.Y - o.Y == 0) {
            return this.X - o.X;
        }

        return this.Y - o.Y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (X != point.X) return false;
        return Y == point.Y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(X, Y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "X=" + X +
                ", Y=" + Y +
                '}';
    }
}
