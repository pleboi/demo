package net.resiststan.algorithms.search;

import java.util.ArrayList;
import java.util.List;

public class Linear<T> implements Search<T> {
    @Override
    public T findFirst(T t, List<T> list) {
        for (T val : list) {
            if (t.equals(val)) {
                return val;
            }
        }

        return null;
    }

    @Override
    public List<T> findAll(T t, List<T> list) {
        List<T> resList = new ArrayList<>();
        for (T val : list) {
            if (t.equals(val)) {
                resList.add(val);
            }
        }

        return resList;
    }
}
