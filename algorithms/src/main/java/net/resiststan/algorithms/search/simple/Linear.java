package net.resiststan.algorithms.search.simple;

public class Linear implements Search {
    @Override
    public int findIn(int elem, int[] a) {
        for (int i = 0; i < a.length; i++){
            if (a[i] == elem) {
                return i;
            }
        }

        return -1;
    }
}
