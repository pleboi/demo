package net.resiststan.algorithms.search;

import java.util.List;

public interface Search<T> {
    static <T> T findFirst(Search<T> search, List<T> list, T t) {
        return search.findFirst(t, list);
    }

    static <T> List<T> findAll(Search<T> search, List<T> list, T t) {
        return search.findAll(t, list);
    }

    T findFirst(T t, List<T> list);

    List<T> findAll(T t, List<T> list);
}
