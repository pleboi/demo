package net.resiststan.algorithms.search.simple;

public interface Search {
    static int find(int[] a, int elem) {
        return Search.find(elem, a, new Bin());

    }

    static int find(int elem, int[] a, Search search) {
        return search.findIn(elem, a);
    }

    int findIn(int elem, int[] a);
}
