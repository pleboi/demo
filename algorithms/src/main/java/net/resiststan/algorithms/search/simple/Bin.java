package net.resiststan.algorithms.search.simple;

public class Bin implements Search {
    @Override
    public int findIn(int elem, int[] a) {
        if (a.length < 1 || elem < a[0] || a[a.length - 1] < elem) {
            return -1;
        }

        return tryFind(a, 0, a.length - 1, elem);
    }

    private int tryFind(int[] a, int l, int r, int elem) {
        int mid = l + (r - l) / 2;

        if (a[mid] == elem) {
            return mid;
        }

        if (l == r) {
            return -1;
        }

        if (elem < a[mid]) {
            return tryFind(a, l, mid - 1, elem);
        }

        return tryFind(a, mid + 1, r, elem);
    }
}
