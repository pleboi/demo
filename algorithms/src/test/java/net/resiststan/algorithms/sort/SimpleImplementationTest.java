package net.resiststan.algorithms.sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SimpleImplementationTest {
    @Parameter(value = 0)
    public Method method;
    @Parameter(value = 1)
    public List<int[]> testData;

    @Parameters(name = "{index}: sort {1}")
    public static Collection<Object[]> data() {
        return getData();
    }

    private static List<Object[]> getData() {
        var o = new ArrayList<Object[]>();

        Arrays.stream(SimpleImplementation.class.getDeclaredMethods())
                .filter(method -> method.canAccess(null))
                .collect(Collectors.toList())
                .forEach(m -> o.add(new Object[]{m, getGenTestData()}));

        return o;
    }

    private static List<int[]> getGenTestData() {
        return Arrays.asList(
                new int[]{1, 2, 3, 4, 5},
                new int[]{5, 4, 3, 2, 1},
                new int[]{5, 1, 43, 8, 7, 12, 3, 2, 4},
                new int[]{1, 2, 3, 1, 2, 4, 2, 1},
                new int[]{1, 2, 3, -2, 4, 5, -1, -1, 0},
                new Random().ints(0, 1000).limit(50).toArray(),
                new Random().ints(0, 20).limit(50).toArray()
        );
    }

    @Test
    public void allSortTest() throws InvocationTargetException, IllegalAccessException {
        System.out.println("sort by method " + method.getName());
        for (int[] arr : testData) {
            int[] expArr = Arrays.copyOf(arr, arr.length);
            Arrays.sort(expArr);

            System.out.println("exp arr: " + Arrays.toString(expArr));
            System.out.println("before sort: " + Arrays.toString(arr));

            method.invoke(null, arr);

            System.out.println("after sort: " + Arrays.toString(arr));

            assertArrayEquals(expArr, arr);

        }
    }
}
