package net.resiststan.dao.postgres;

import net.resiststan.dao.base.BaseCrud;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.base.DaoFactory;
import net.resiststan.entity.Department;
import net.resiststan.entity.Location;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class DepartmentsDaoTest {
    private static BaseCrud<Department, Long> dao;
    private static Connection connection;

    @BeforeClass
    public static void setUp() throws DaoException, SQLException {
        DataSource dataSource = new PostgresDataSource().getDataSource();
        connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        DaoFactory daoFactory = new PostgresDaoFactory();
        dao = daoFactory.getDao(Department.class, connection);
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    @After
    public void rollbackAfterAnyTest() throws SQLException {
        connection.rollback();
    }

    @Test
    public void createTest() throws DaoException {
        Department department1 = createDepartment(1);

        Department newDep = dao.save(department1);

        assertEquals(department1.getName(), newDep.getName());
        assertEquals(department1.getDescription(), newDep.getDescription());
        assertEquals(department1.getLocation(), newDep.getLocation());
        assertNotNull("ID can not be null", newDep.getId());
    }

    @Test
    public void selectAllQueryTest() throws DaoException {
        int departmentsCountBefore = dao.getAll().size();

        Department department1 = createDepartment(1);
        Department department2 = createDepartment(2);
        Department department3 = createDepartment(3);

        dao.save(department1);
        dao.save(department2);
        dao.save(department3);

        List<Department> departmentList = dao.getAll();

        assertEquals(departmentsCountBefore + 3, departmentList.size());
    }

    @Test
    public void deleteTest() throws DaoException {
        Department savedObject = dao.save(createDepartment(1));

        int departmentsCountBefore = dao.getAll().size();

        dao.delete(savedObject.getId());

        assertEquals(departmentsCountBefore - 1, dao.getAll().size());
        assertNull(dao.find(savedObject.getId()));
    }

    @Test
    public void findByIdTest() throws DaoException {
        Department department1 = createDepartment(1);
        Department savedObject = dao.save(department1);

        Department foundObject = dao.find(savedObject.getId());

        assertNotNull(foundObject);
        assertEquals(savedObject.getId(), foundObject.getId());
        assertEquals(savedObject.getName(), foundObject.getName());
        assertEquals(savedObject.getDescription(), foundObject.getDescription());
    }

    @Test
    public void updateTest() throws DaoException {
        Department departmentSrc = dao.save(createDepartment(1));

        dao.update(departmentSrc.getId(), departmentSrc);

        Department departmentAfterUpdate = dao.find(departmentSrc.getId());

        assertEquals(departmentSrc.getName(), departmentAfterUpdate.getName());
        assertEquals(departmentSrc.getDescription(), departmentAfterUpdate.getDescription());
    }

    private Department createDepartment(int number) {
        return Department.builder()
                .name("test name department " + number)
                .description("description")
                .location(new Location(new Location.Angle(43, 117312), new Location.Angle(131, 912648)))
                .build();
    }
}