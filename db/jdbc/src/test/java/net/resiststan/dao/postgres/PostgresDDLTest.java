package net.resiststan.dao.postgres;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.sql.DataSource;

public class PostgresDDLTest {
    private static DataSource ds;

    @BeforeClass
    public static void loadDataSource() {
        ds = new PostgresDataSource().getDataSource();
    }

    @Test
    public void ddlTest() {
        new PostgresDDL(ds).create();
    }
}