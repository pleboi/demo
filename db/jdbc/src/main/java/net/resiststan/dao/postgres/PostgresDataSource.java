package net.resiststan.dao.postgres;

import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;

public class PostgresDataSource {
    private static final String DEFAULT_SERVER_NAME = "localhost";
    private static final int DEFAULT_PORT = 5432;
    private static final String DEFAULT_DB_NAME = "demo";
    private static final String DEFAULT_USERNAME = "postgres";
    private static final String DEFAULT_PASSWORD = "root";

    private final PGSimpleDataSource dataSource;

    public PostgresDataSource() {
        this(DEFAULT_SERVER_NAME, DEFAULT_PORT, DEFAULT_DB_NAME, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public PostgresDataSource(String serverName, int port, String dbName, String username, String password) {
        dataSource = new PGSimpleDataSource();
        dataSource.setServerName(serverName);
        dataSource.setPortNumber(port);
        dataSource.setDatabaseName(dbName);
        dataSource.setUser(username);
        dataSource.setPassword(password);
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
