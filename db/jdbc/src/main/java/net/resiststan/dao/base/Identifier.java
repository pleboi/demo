package net.resiststan.dao.base;

public interface Identifier<PK> {
    PK getId();
}
