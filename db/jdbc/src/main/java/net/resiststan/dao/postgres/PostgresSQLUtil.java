package net.resiststan.dao.postgres;

public interface PostgresSQLUtil {
    default String getLastIdSql(String table, String field) {
        return String.format("(SELECT CURRVAL(pg_get_serial_sequence('%s', '%s')))", table, field);
    }

    default String deleteSql(String table, String field) {
        return String.format("DELETE FROM %s WHERE %s = ?", table, field);
    }

    default String selectBySql(String table, String field) {
        return String.format("SELECT * FROM %s WHERE %s = ?", table, field);
    }

    default String selectAllSql(String table) {
        return String.format("SELECT * FROM %s", table);
    }
}
