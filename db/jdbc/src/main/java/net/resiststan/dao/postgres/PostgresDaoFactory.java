package net.resiststan.dao.postgres;

import net.resiststan.dao.base.BaseCrud;
import net.resiststan.dao.base.BaseDao;
import net.resiststan.dao.base.DaoException;
import net.resiststan.dao.base.DaoFactory;
import net.resiststan.dao.postgres.dao.DepartmentsDao;
import net.resiststan.dao.postgres.dao.EmployeesDao;
import net.resiststan.dao.postgres.dao.PositionsDao;
import net.resiststan.dao.postgres.dao.TasksDao;
import net.resiststan.entity.Department;
import net.resiststan.entity.Employee;
import net.resiststan.entity.Position;
import net.resiststan.entity.Task;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class PostgresDaoFactory implements DaoFactory {
    private final Map<Class, BaseDao> daoMap;

    public PostgresDaoFactory() {
        this.daoMap = new HashMap<>();

        daoMap.put(Department.class, new DepartmentsDao());
        daoMap.put(Position.class, new PositionsDao());
        daoMap.put(Employee.class, new EmployeesDao());
        daoMap.put(Task.class, new TasksDao());
    }

    @Override
    public BaseCrud getDao(Class entityClass, Connection connection) throws DaoException {
        if (daoMap.containsKey(entityClass)) {
            BaseDao baseDao = daoMap.get(entityClass);
            baseDao.setConnection(connection);
            return baseDao;
        }

        throw new DaoException("Dao object for " + entityClass + " not provided.");
    }
}
