package net.resiststan.dao.base;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public abstract class BaseDao<T, PK> implements BaseCrud<T, PK>, ConnectionKeeper {
    private Connection connection;

    public BaseDao(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    protected abstract String getInsertOneQuery();

    protected abstract String getSelectByPrimaryKeyQuery();

    protected abstract String getSelectAllQuery();

    protected abstract String getUpdateByPrimaryKeyQuery();

    protected abstract String getDeleteByPrimaryKeyQuery();

    protected abstract List<T> parseResultSet(ResultSet rs) throws DaoException;

    protected abstract String getLastInsertPK();

    @Override
    public T save(T t) throws DaoException {
        if (t == null) {
            throw new DaoException("Object can not be null.");
        }

        T persistInstance;
        String sql = getInsertOneQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, t);
            int count = statement.executeUpdate();

            if (count != 1) {
                throw new DaoException("On persist modify more then 1 record: " + count);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        sql = getSelectAllQuery() + " WHERE id = " + getLastInsertPK() + ";";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            List<T> list = parseResultSet(rs);
            if ((list == null) || (list.size() != 1)) {
                throw new DaoException("Exception on findByPK new persist data.");
            }
            persistInstance = list.iterator().next();
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return persistInstance;
    }

    protected abstract void prepareStatementForInsert(PreparedStatement statement, T t) throws DaoException;

    @Override
    public T find(PK pk) throws DaoException {
        List<T> list;
        String sql = getSelectByPrimaryKeyQuery();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setObject(1, pk);
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        }

        if (list == null || list.size() == 0) {
            return null;
        }

        if (list.size() > 1) {
            throw new DaoException("Received more than one record.");
        }

        return list.iterator().next();
    }

    @Override
    public List<T> getAll() throws DaoException {
        List<T> list;
        String sql = getSelectAllQuery();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return list;
    }

    @Override
    public void update(PK pk, T t) throws DaoException {
        String sql = getUpdateByPrimaryKeyQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForUpdate(statement, t, pk);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("On update modify more then 1 record: " + count);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T t, PK pk) throws DaoException;

    @Override
    public void delete(PK pk) throws DaoException {
        String sql = getDeleteByPrimaryKeyQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            try {
                statement.setObject(1, pk);
            } catch (Exception e) {
                throw new DaoException(e);
            }

            int count = statement.executeUpdate();

            if (count != 1) {
                throw new DaoException("On delete modify more then 1 record: " + count);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
}
