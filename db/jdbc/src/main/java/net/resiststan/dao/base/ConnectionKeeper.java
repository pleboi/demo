package net.resiststan.dao.base;

import java.sql.Connection;

public interface ConnectionKeeper {
    void setConnection(Connection connection);

    Connection getConnection();
}
