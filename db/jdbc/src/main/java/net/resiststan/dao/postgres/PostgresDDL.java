package net.resiststan.dao.postgres;

import net.resiststan.dao.base.DBData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class PostgresDDL extends DBData {
    private final static String SCHEME_NAME = "jdbcdemo";

    PostgresDDL(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public void create(Connection connection) {

        try (Statement statement = connection.createStatement()) {
            createSchema(statement);
            createTableDepartments(statement);
            createTablePositions(statement);
            createTableEmployees(statement);
            createTableTasks(statement);
            createTableEmployeesTasksRelations(statement);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createSchema(Statement statement) throws SQLException {
        String sql = "CREATE SCHEMA IF NOT EXISTS " + SCHEME_NAME + ";";

        LOG.info(sql);

        statement.executeUpdate(sql);
    }

    private void createTableDepartments(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + DEPARTMENTS_TABLE.TABLE_NAME + " (" +
                DEPARTMENTS_TABLE.COLUMNS.ID + " int not null generated always as identity, " +
                DEPARTMENTS_TABLE.COLUMNS.NAME + " varchar(100) not null, " +
                DEPARTMENTS_TABLE.COLUMNS.DESCRIPTION + " varchar(100), " +
                DEPARTMENTS_TABLE.COLUMNS.LOCATION + " varchar(100) not null, " +
                "primary key (" + DEPARTMENTS_TABLE.COLUMNS.ID + ")" +
                ");";


        LOG.info(sql);

        statement.executeUpdate(sql);
    }

    private void createTablePositions(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + POSITIONS_TABLE.TABLE_NAME + " (" +
                POSITIONS_TABLE.COLUMNS.ID + " int not null generated always as identity, " +
                POSITIONS_TABLE.COLUMNS.NAME + " varchar(100) not null, " +
                "primary key (" + POSITIONS_TABLE.COLUMNS.ID + ")" +
                ");";

        LOG.info(sql);

        statement.executeUpdate(sql);
    }

    private void createTableEmployees(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + EMPLOYEES_TABLE.TABLE_NAME + " (" +
                EMPLOYEES_TABLE.COLUMNS.ID + " int not null generated always as identity, " +
                EMPLOYEES_TABLE.COLUMNS.FIRST_NAME + " varchar(100) not null, " +
                EMPLOYEES_TABLE.COLUMNS.LAST_NAME + " varchar(100) not null, " +
                EMPLOYEES_TABLE.COLUMNS.POSITION_ID + " int not null, " +
                EMPLOYEES_TABLE.COLUMNS.DEPARTMENT_ID + " int not null, " +
                "primary key (" + EMPLOYEES_TABLE.COLUMNS.ID + ")," +
                "UNIQUE (" + EMPLOYEES_TABLE.COLUMNS.POSITION_ID + "), " +
                "FOREIGN KEY (" + EMPLOYEES_TABLE.COLUMNS.POSITION_ID + ") REFERENCES " + POSITIONS_TABLE.TABLE_NAME + "(" + POSITIONS_TABLE.COLUMNS.ID + "), " +
                "FOREIGN KEY (" + EMPLOYEES_TABLE.COLUMNS.DEPARTMENT_ID + ") REFERENCES " + DEPARTMENTS_TABLE.TABLE_NAME + "(" + DEPARTMENTS_TABLE.COLUMNS.ID + ")" +
                ");";

        LOG.info(sql);

        statement.executeUpdate(sql);
    }

    private void createTableTasks(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + TASKS_TABLE.TABLE_NAME + " (" +
                TASKS_TABLE.COLUMNS.ID + " int not null generated always as identity, " +
                TASKS_TABLE.COLUMNS.DESCRIPTION + " varchar(100) not null, " +
                "primary key (" + TASKS_TABLE.COLUMNS.ID + ")" +
                ");";

        LOG.info(sql);

        statement.executeUpdate(sql);
    }

    private void createTableEmployeesTasksRelations(Statement statement) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + EMPLOYEES_TASKS_TABLE.TABLE_NAME + " (" +
                EMPLOYEES_TASKS_TABLE.COLUMNS.TASK_ID + " int REFERENCES " + TASKS_TABLE.TABLE_NAME + " (" + TASKS_TABLE.COLUMNS.ID + ") ON UPDATE CASCADE ON DELETE CASCADE, " +
                EMPLOYEES_TASKS_TABLE.COLUMNS.EMPLOYEE_ID + " int REFERENCES " + EMPLOYEES_TABLE.TABLE_NAME + " (" + EMPLOYEES_TABLE.COLUMNS.ID + ") ON UPDATE CASCADE ON DELETE CASCADE, " +
                "CONSTRAINT " + EMPLOYEES_TASKS_TABLE.CONSTRAINT + " PRIMARY KEY (" + EMPLOYEES_TASKS_TABLE.COLUMNS.TASK_ID + ", " + EMPLOYEES_TASKS_TABLE.COLUMNS.EMPLOYEE_ID + ")" +
                ");";

        LOG.info(sql);

        statement.executeUpdate(sql);
    }

    public interface DEPARTMENTS_TABLE {
        String TABLE_NAME = getTableName("Departments");

        interface COLUMNS {
            String ID = "id";
            String NAME = "name";
            String DESCRIPTION = "description";
            String LOCATION = "location";
        }
    }

    public interface EMPLOYEES_TABLE {
        String TABLE_NAME = getTableName("Employees");

        interface COLUMNS {
            String ID = "id";
            String FIRST_NAME = "first_name";
            String LAST_NAME = "last_name";
            String DEPARTMENT_ID = "department_id";
            String POSITION_ID = "position_id";
        }
    }

    public interface POSITIONS_TABLE {
        String TABLE_NAME = getTableName("Positions");

        interface COLUMNS {
            String ID = "id";
            String NAME = "name";
        }
    }

    public interface TASKS_TABLE {
        String TABLE_NAME = getTableName("Tasks");

        interface COLUMNS {
            String ID = "id";
            String DESCRIPTION = "description";
        }
    }

    public interface EMPLOYEES_TASKS_TABLE {
        String TABLE_NAME = getTableName("Employees_Tasks");

        String CONSTRAINT = "Employees_Tasks_pkey";

        interface COLUMNS {
            String EMPLOYEE_ID = "employee_id";
            String TASK_ID = "task_id";
        }
    }

    private static String getTableName(String name) {
        return SCHEME_NAME == null || SCHEME_NAME.isBlank() ? name : SCHEME_NAME + "." + name;
    }
}