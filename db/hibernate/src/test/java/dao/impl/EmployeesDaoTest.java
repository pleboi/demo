package dao.impl;

import dao.BaseCrud;
import dao.BaseDaoFactory;
import entity.Department;
import entity.Employee;
import entity.Location;
import entity.Position;
import entity.Task;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EmployeesDaoTest {
    private static BaseCrud<Employee, Long> employeesDao;

    private static List<Employee> employees;
    private static List<Location> locations;
    private static List<Position> positions;
    private static List<Department> departments;
    private static List<Task> tasks;

    private static BaseDaoFactory baseDaoFactory;

    @BeforeClass
    public static void setUp() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

        baseDaoFactory = new BaseDaoFactory(sessionFactory);
        employeesDao = baseDaoFactory.getDao(Employee.class);

        initData();
    }

    @Test
    public void saveTest() {
        Employee expectEmployee = employees.get(0);
        Employee actualEmployee = employeesDao.save(expectEmployee);

        assertNotNull(actualEmployee.getId());
        assertEquals(expectEmployee.getDepartment(), actualEmployee.getDepartment());
        assertEquals(expectEmployee.getPosition(), actualEmployee.getPosition());
        assertEquals(expectEmployee.getFirstName(), actualEmployee.getFirstName());
        assertEquals(expectEmployee.getLastName(), actualEmployee.getLastName());

        Set<Task> expectEmployeeTasks = expectEmployee.getTasks();
        Set<Task> actualEmployeeTasks = actualEmployee.getTasks();

        assertEquals(expectEmployeeTasks.size(), actualEmployeeTasks.size());
        assertTrue(actualEmployeeTasks.containsAll(expectEmployeeTasks));
    }

    @Test
    public void findTest() {
        Employee expectEmployee = employees.get(1);

        employeesDao.save(expectEmployee);
        employeesDao.save(employees.get(2));

        Long expectId = 2L;
        Employee actualEmployee = employeesDao.find(expectId);

        assertNotNull(actualEmployee.getId());
        assertEquals(expectEmployee.getDepartment(), actualEmployee.getDepartment());
        assertEquals(expectEmployee.getPosition(), actualEmployee.getPosition());
        assertEquals(expectEmployee.getFirstName(), actualEmployee.getFirstName());
        assertEquals(expectEmployee.getLastName(), actualEmployee.getLastName());

        Set<Task> expectEmployeeTasks = expectEmployee.getTasks();
        Set<Task> actualEmployeeTasks = actualEmployee.getTasks();

        assertEquals(expectEmployeeTasks.size(), actualEmployeeTasks.size());
        assertTrue(actualEmployeeTasks.containsAll(expectEmployeeTasks));
    }

    @Test
    public void getAllTest() {
        List<Employee> actualEmployees = employeesDao.getAll();

        assertEquals(3, actualEmployees.size());
    }

    @Test
    public void updateTest() {
        Employee actualEmployee = employeesDao.find(3L);
        actualEmployee.setFirstName("Kirk");

        employeesDao.update(actualEmployee);
        Employee expectEmployee = employeesDao.find(3L);

        assertEquals(expectEmployee.getId(), actualEmployee.getId());
        assertEquals(expectEmployee.getFirstName(), actualEmployee.getFirstName());
    }

    @Test
    public void deleteTest() {
        Employee deleteEmployee = employeesDao.find(3L);
        employeesDao.delete(deleteEmployee);
        List<Employee> actualTasks = employeesDao.getAll();

        Employee employeeWithId3 = employeesDao.find(3L);

        assertEquals(2, actualTasks.size());
        assertNull(employeeWithId3);
    }

    private static void initData() {
        initLocations();
        initDepartments();
        initPositions();
        initTasks();

        employees = Arrays.asList(
                Employee.builder().firstName("Mari").lastName("Ross").position(positions.get(0)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Nick").lastName("Moss").position(positions.get(1)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Sho").lastName("Brown").position(positions.get(2)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Geek").lastName("Break").position(positions.get(3)).department(departments.get(0)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Roma").lastName("Rick").position(positions.get(4)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Ann").lastName("Bell").position(positions.get(5)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Fill").lastName("Car").position(positions.get(6)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lana").lastName("Don").position(positions.get(7)).department(departments.get(1)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Kira").lastName("Hill").position(positions.get(8)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Zak").lastName("Ver").position(positions.get(9)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lira").lastName("Age").position(positions.get(10)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("John").lastName("Tor").position(positions.get(11)).department(departments.get(2)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Lisa").lastName("Mur").position(positions.get(12)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Tom").lastName("Rod").position(positions.get(13)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lily").lastName("Pain").position(positions.get(14)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Lom").lastName("Save").position(positions.get(15)).department(departments.get(3)).tasks(Set.copyOf(tasks)).build(),

                Employee.builder().firstName("Ron").lastName("Guns").position(positions.get(16)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Helga").lastName("Fi").position(positions.get(17)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Mick").lastName("Ruk").position(positions.get(18)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build(),
                Employee.builder().firstName("Zoi").lastName("Will").position(positions.get(19)).department(departments.get(4)).tasks(Set.copyOf(tasks)).build()
        );
    }

    private static void initLocations() {
        locations = Arrays.asList(
                new Location(new Location.Angle(69, 191466), new Location.Angle(33, 235617)),
                new Location(new Location.Angle(43, 587769), new Location.Angle(39, 736788)),
                new Location(new Location.Angle(54, 722154), new Location.Angle(20, 486398)),
                new Location(new Location.Angle(43, 117312), new Location.Angle(131, 912648))
        );
    }

    private static void initDepartments() {
        departments = Arrays.asList(
                Department.builder().name("Main Department").location(locations.get(2)).description("Main Department").build(),
                Department.builder().name("North Department").location(locations.get(0)).build(),
                Department.builder().name("South Department").location(locations.get(1)).build(),
                Department.builder().name("West Department").location(locations.get(2)).build(),
                Department.builder().name("East Department").location(locations.get(3)).build()
        );

        saveAll(Department.class, departments);
    }

    private static void initPositions() {
        positions = Arrays.asList(
                Position.builder().name("Director of Main Department").build(),
                Position.builder().name("Assistant of Main Department").build(),
                Position.builder().name("Resource allocator of Main Department").build(),
                Position.builder().name("Security officer of Main Department").build(),

                Position.builder().name("Director of North Department").build(),
                Position.builder().name("Assistant of North Department").build(),
                Position.builder().name("Resource allocator of North Department").build(),
                Position.builder().name("Security officer of North Department").build(),

                Position.builder().name("Director of South Department").build(),
                Position.builder().name("Assistant of South Department").build(),
                Position.builder().name("Resource allocator of South Department").build(),
                Position.builder().name("Security officer of South Department").build(),

                Position.builder().name("Director of West Department").build(),
                Position.builder().name("Assistant of West Department").build(),
                Position.builder().name("Resource allocator of West Department").build(),
                Position.builder().name("Security officer of West Department").build(),

                Position.builder().name("Director of East Department").build(),
                Position.builder().name("Assistant of East Department").build(),
                Position.builder().name("Resource allocator of East Department").build(),
                Position.builder().name("Security officer of East Department").build()
        );

        saveAll(Position.class, positions);
    }

    private static void initTasks() {
        tasks = Arrays.asList(
                Task.builder().description("to be on the same wavelength with colleagues").build(),
                Task.builder().description("have a good mood").build(),
                Task.builder().description("make the world a better place").build()
        );

        saveAll(Task.class, tasks);
    }

    private static <T> void saveAll(Class dtoClass, List<T> list) {
        BaseCrud<T, Long> dao = baseDaoFactory.getDao(dtoClass);

        for (int i = 0; i < list.size(); i++) {
            T save = dao.save(list.get(i));
            list.set(i, save);
        }
    }
}
