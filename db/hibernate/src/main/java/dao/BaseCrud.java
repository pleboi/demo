package dao;

import java.io.Serializable;
import java.util.List;

public interface BaseCrud<E, PK extends Serializable> {
    E save(E e);

    E find(PK pk);

    List<E> getAll();

    void update(E e);

    void delete(E e);
}
