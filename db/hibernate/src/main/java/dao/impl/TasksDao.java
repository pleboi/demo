package dao.impl;

import dao.BaseDao;
import entity.Task;
import org.hibernate.SessionFactory;

public class TasksDao extends BaseDao<Task, Long> {
    public TasksDao(SessionFactory factory) {
        super(factory);
    }
}
