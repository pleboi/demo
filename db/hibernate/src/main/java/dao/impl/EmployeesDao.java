package dao.impl;

import dao.BaseDao;
import entity.Employee;
import org.hibernate.SessionFactory;

public class EmployeesDao extends BaseDao<Employee, Long> {
    public EmployeesDao(SessionFactory factory) {
        super(factory);
    }
}
