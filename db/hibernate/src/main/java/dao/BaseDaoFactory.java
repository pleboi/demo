package dao;

import dao.impl.DepartmentsDao;
import dao.impl.EmployeesDao;
import dao.impl.PositionsDao;
import dao.impl.TasksDao;
import entity.Department;
import entity.Employee;
import entity.Position;
import entity.Task;
import org.hibernate.SessionFactory;

import java.util.HashMap;
import java.util.Map;

public class BaseDaoFactory {
    private final Map<Class, BaseDao> daoMap;

    public BaseDaoFactory(SessionFactory sessionFactory) {
        this.daoMap = new HashMap<>();

        daoMap.put(Department.class, new DepartmentsDao(sessionFactory));
        daoMap.put(Position.class, new PositionsDao(sessionFactory));
        daoMap.put(Employee.class, new EmployeesDao(sessionFactory));
        daoMap.put(Task.class, new TasksDao(sessionFactory));
    }

    public <E> BaseCrud getDao(E entityClass) {
        if (daoMap.containsKey(entityClass)) {
            return daoMap.get(entityClass);
        }

        return null;
    }
}
