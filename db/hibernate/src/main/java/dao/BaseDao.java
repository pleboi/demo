package dao;

import com.sun.istack.NotNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class BaseDao<E, PK extends Serializable> implements BaseCrud<E, PK> {
    protected final SessionFactory factory;
    private final Class<E> type;

    @SuppressWarnings("unchecked")
    public BaseDao(@NotNull final SessionFactory factory) {
        this.type = (Class<E>)
                ((ParameterizedType) getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[0];

        this.factory = factory;
    }

    @Override
    public E save(E e) {
        E saved;

        try (final Session session = factory.openSession()) {
            session.beginTransaction();

            PK pk = (PK) session.save(e);
            saved = (E) session.load(e.getClass(), (Serializable) pk);

            session.getTransaction().commit();
        }

        return saved;
    }

    @Override
    public E find(PK pk) {
        try (final Session session = factory.openSession()) {
            return (E) session.get(type, pk);
        }
    }

    @Override
    public void update(E e) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();

            session.update(e);

            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(E e) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();

            session.delete(e);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<E> getAll() {
        try (final Session session = factory.openSession()) {
            return session.createQuery("FROM entity." + type.getSimpleName()).list();
        }
    }
}
