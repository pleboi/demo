package utils.usertypes;

import entity.Location;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;
import java.util.regex.Pattern;

public class LocationType implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[]{Types.VARCHAR};
    }

    @Override
    public Class returnedClass() {
        return Location.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        if (rs.wasNull()) {
            return null;
        }

        return locationFrom(rs.getString(names[0]));
    }

    private Location locationFrom(String string) {
        if (!string.isEmpty()) {
            Pattern pattern = Pattern.compile("\\D");
            String[] strings = pattern.split(string, 4);

            if (strings.length == 4) {
                try {
                    Location.Angle latitude = new Location.Angle(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
                    Location.Angle longitude = new Location.Angle(Integer.parseInt(strings[2]), Integer.parseInt(strings[3]));

                    return new Location(latitude, longitude);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if (Objects.isNull(value)) {
            st.setNull(index, Types.VARCHAR);
        } else {
            st.setObject(index, stringFrom((Location) value));
        }
    }

    private String stringFrom(Location location) {
        Location.Angle longitude = location.getLongitude();
        Location.Angle latitude = location.getLatitude();
        return String.format("%s,%s;%s,%s", latitude.getD1(), latitude.getD2(), longitude.getD1(), longitude.getD2());
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
