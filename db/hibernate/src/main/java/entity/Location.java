package entity;

import lombok.Data;

@Data
public class Location {
    private final Angle latitude;
    private final Angle longitude;

    @Data
    public static class Angle {
        private final int d1;
        private final int d2;
    }
}
