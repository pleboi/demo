import configuration.TestAppConfig;
import entity.Department;
import entity.Employee;
import entity.Position;
import entity.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import repository.DepartmentRepository;
import repository.EmployeeRepository;
import repository.TaskRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.StreamSupport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestAppConfig.class)
@Transactional
@Rollback
@Sql({"/init_test_data.sql"})
public class EmployeeRepositoryTest {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Test
    public void saveAllTest() {
        long countExpect = 20;
        long countActual = employeeRepository.count();
        assertEquals(countExpect, countActual);
    }

    @Test
    public void findAllTest() {
        long countExpect = 20;
        long countActual = StreamSupport.stream(employeeRepository.findAll().spliterator(), true).count();
        assertEquals(countExpect, countActual);
    }

    @Test
    public void existsByIdTest() {
        boolean existsActual = employeeRepository.existsById(1L);
        assertTrue(existsActual);
    }

    @Test
    public void deleteAllTest() {
        int countExpect = 0;
        employeeRepository.deleteAll();
        assertEquals(countExpect, employeeRepository.count());
    }

    @Test
    public void deleteByIdTest() {
        int countExpect = 19;
        long id = 3L;

        employeeRepository.deleteById(id);
        Optional<Employee> employee = employeeRepository.findById(id);

        assertEquals(countExpect, employeeRepository.count());
        assertTrue(employee.isEmpty());
    }

    @Test
    public void saveNewTest() {
        Set<Task> tasks = getTasks();
        Employee srcEmployee = Employee.builder()
                .firstName("Geek")
                .lastName("Break")
                .position(Position.builder().name("trainee").build())
                .department(getDepartment(5L))
                .tasks(tasks)
                .build();

        Employee savedEmployee = employeeRepository.save(srcEmployee);
        assertNotNull(savedEmployee.getId());

        Optional<Employee> actualEmployee = employeeRepository.findById(savedEmployee.getId());
        assertEquals(savedEmployee, actualEmployee.get());
    }

    @Test
    public void saveExistTest() {
        String expectFirstName = "Lola";
        long employeeId = 9L;

        Optional<Employee> employee = employeeRepository.findById(employeeId);
        employee.ifPresent(e -> {
            e.setFirstName(expectFirstName);
            employeeRepository.save(e);
        });

        Optional<Employee> actualEmployee = employeeRepository.findById(employeeId);

        assertEquals(expectFirstName, actualEmployee.get().getFirstName());
    }

    @Test
    public void findByIdTest() {
        Long expectId = 5L;
        Optional<Employee> employee = employeeRepository.findById(expectId);

        assertFalse(employee.isEmpty());
        assertEquals(expectId, employee.get().getId());
    }

    @Test
    public void deleteTest() {
        int countExpect = 19;
        long expectId = 7L;

        employeeRepository.findById(expectId).ifPresent(employeeRepository::delete);

        assertEquals(countExpect, employeeRepository.count());
    }

    private Set<Task> getTasks() {
        return new HashSet<>((Collection<? extends Task>) taskRepository.findAll());
    }

    private Department getDepartment(Long id) {
        return departmentRepository.findById(id)
                .orElse(Department.builder().name("undefined department").build());
    }
}